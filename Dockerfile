FROM python:3.9-slim-bullseye

############
## Docker ##
############
ARG DOCKER_CHANNEL=stable
ARG DOCKER_VERSION=20.10.12

RUN apt-get update && apt-get install -y \
      --no-install-recommends \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg2 \
      software-properties-common \
 && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
 && add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   ${DOCKER_CHANNEL}" \
 && apt-get update && apt-get install -y \
    --no-install-recommends \
    docker-ce=5:${DOCKER_VERSION}~3-0~debian-bullseye \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && docker --version \
 && dockerd --version

####################
## Docker Compose ##
####################
ARG DOCKER_COMPOSE_VERSION=2.2.3
RUN curl -fL --no-progress-meter -o /bin/docker-compose "https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION}/docker-compose-linux-x86_64" \
 && chmod +x /bin/docker-compose \
 && mkdir -p /usr/local/lib/docker/cli-plugins/ \
 && ln -s /bin/docker-compose /usr/local/lib/docker/cli-plugins/ \
 && docker compose version

#################
## DIND Script ##
#################
ENV DOCKER_EXTRA_OPTS '--storage-driver=overlay2'
RUN curl -fL --no-progress-meter -o /usr/local/bin/dind "https://raw.githubusercontent.com/moby/moby/v${DOCKER_VERSION}/hack/dind" \
 && chmod +x /usr/local/bin/dind


VOLUME /var/lib/docker
EXPOSE 2375 2376
